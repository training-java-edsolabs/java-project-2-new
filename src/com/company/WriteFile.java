package com.company;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class WriteFile {
    public static Random rand = new Random();
    public WriteFile(Integer number){
        File myObj = new File("input.txt");//Create File
        try {
            FileWriter myWriter = new FileWriter("input.txt");//Write file created above
            for (int i = 0; i < number; i++) {
                myWriter.write(String.valueOf(rand.nextInt(number)));
                myWriter.write(" ");
            }
            myWriter.close();
            System.out.println("File direction:"+myObj.getAbsolutePath());
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
