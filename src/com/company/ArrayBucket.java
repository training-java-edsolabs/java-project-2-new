package com.company;

public class ArrayBucket implements Comparable<ArrayBucket>{
    int[] arr;
    int index;
    public ArrayBucket(int[] arr, int index){
        this.arr = arr;
        this.index = index;
    }

    @Override
    public int compareTo(ArrayBucket o) {
        return this.arr[this.index] - o.arr[o.index];
    }
}
