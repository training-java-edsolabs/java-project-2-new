package com.company;

import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class ReadFile {
    public ReadFile(String direction, Integer numberThread) throws IOException, ExecutionException, InterruptedException {
        //read file input.txt, divide numbers to n thread and use radix sort sorting numbers,
        //write file to output.txt
        //đọc file
        FileReader fr = new FileReader(direction);
        BufferedReader reader = new BufferedReader(fr);
        ArrayList<String> str = new ArrayList<>();
        String line = "";
        //Lưu file vào dãy mới để sort
        ArrayList<Integer> numbers = new ArrayList<>();
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(" ");
            numbers.addAll(Arrays.stream(data).map(Integer::parseInt).collect(Collectors.toList()));
        }

        //chỗ xử lý multithreading
        Map<Integer, List<Integer>> mapList = new HashMap<>();

        for (int i = 0; i < numberThread; i++) {
            List<Integer> half = numbers.subList((i * numbers.size()) / numberThread, ((i + 1) * numbers.size()) / numberThread);
            mapList.put(i, half);
        }

        ExecutorService executor = Executors.newFixedThreadPool(numberThread);

        List<Future> tasks = new ArrayList<>();

        for (int i = 0; i < numberThread; i++) {
            Future<List<Integer>> task = executor.submit(new RequestHandler(mapList.get(i)));
            tasks.add(task);
        }

        for (int i = 0; i < numberThread; i++) {
            tasks.get(i).get();
        }


        int[][] arr2d = tasks.stream()
                .map(l -> {
                    try {
                        List<Integer> list = (List<Integer>) l.get();
                        return list.stream().mapToInt(Integer::intValue).toArray();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        return null;
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                        return null;
                    }
                }).toArray(int[][]::new);
        int[] result = mergeSortedArray(arr2d);

        FileWriter writer = new FileWriter("output.txt");
        File file = new File("output.txt");
        for (Integer i : result) {
            writer.write(String.valueOf(i));
            writer.write(" ");
        }
        System.out.println("File sorted: " + file.getAbsolutePath());

        writer.close();
    }

    public static int[] mergeSortedArray(int[][] arr){
        PriorityQueue<ArrayBucket> queue = new
                PriorityQueue<ArrayBucket>();
        int total = 0;
        for (int i = 0; i < arr.length; i++){
            queue.add(new ArrayBucket(arr[i], 0));
            total = total + arr[i].length;
        }
        int m = 0;
        int result[] = new int[total];
        while (!queue.isEmpty()){
            ArrayBucket ac = queue.poll();
            result[m++] = ac.arr[ac.index];
            if (ac.index < ac.arr.length - 1){
                queue.add(new ArrayBucket(ac.arr, ac.index + 1));
            }
        }
        return result;
    }
}
