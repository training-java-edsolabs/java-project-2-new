package com.company;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class FileUtil {
    public FileUtil(String link, int lastNumber) {
        this.link = link;
        this.lastNumber = lastNumber;
    }

    public FileUtil() { }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getLastNumber() {
        return lastNumber;
    }

    public void setLastNumber(int lastNumber) {
        this.lastNumber = lastNumber;
    }

    private String link;
    private int lastNumber;

    public void readFile() throws IOException {
        File file = new File(this.link);
        int count = 0;
        int countLastNumberLines = 0;
        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;
        String st1;
        List<String> fileArray = new ArrayList<>();

        while ((st = br.readLine()) != null) fileArray.add(st);

        for (int i = fileArray.size() - this.lastNumber; i < fileArray.size(); i++) {
            String s = new String(fileArray.get(i).getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
            System.out.println(s);
        }

    }
}
